class AppImageAsset {
  static const String rootImages = "assets/images";
  static const String rootLottie = "assets/lottie";
  static const String logo = "$rootImages/logo.png";
  static const String logo2 = "$rootImages/logoapp.png";
  static const String onBoardingimageOne = "$rootImages/one1.PNG";
  static const String onBoardingimageTwo = "$rootImages/two1.PNG";
  static const String onBoardingimageThree = "$rootImages/three1.PNG";
  static const String loading = "$rootLottie/loading.json";
  static const String offline = "$rootLottie/offline.json";
  static const String noData = "$rootLottie/nodata.json";
  static const String server = "$rootLottie/server.json";
  static const String avatar = "$rootImages/avatar.png";
  static const String saleOne = "$rootImages/sale.png";
  static const String deliveryImage2 = "$rootImages/006-delivery.png";
  static const String drivethruImage = "$rootImages/drivethru.png";
  // static const String onBoardingimageFour = "$rootImages/onboardingfour.PNG";
}
